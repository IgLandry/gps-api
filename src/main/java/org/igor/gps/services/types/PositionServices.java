package org.igor.gps.services.types;

import org.igor.gps.model.Position;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface PositionServices {

    Position createPosition(Position position);

    boolean isDifferenceSmallerThan10Km(UUID positionOneId,
                                        UUID positionTwoId);

    List<Position> getAllPositions();

    int deletePosition(UUID positionId);

    boolean existsByLongitudeAndLatitude(BigDecimal longitude, BigDecimal latitude);
}
