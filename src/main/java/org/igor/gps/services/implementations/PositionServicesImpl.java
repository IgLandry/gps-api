package org.igor.gps.services.implementations;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.igor.gps.exception.PositionAlreadyExistsException;
import org.igor.gps.exception.PositionNotFoundException;
import org.igor.gps.mappers.PositionMapper;
import org.igor.gps.model.Position;
import org.igor.gps.repositories.PositionRepository;
import org.igor.gps.services.types.PositionServices;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class PositionServicesImpl implements PositionServices {

    private static final String POSITION_NOT_FOUND = "Position not found!";
    private PositionRepository positionRepository;
    private PositionMapper positionMapper;

    /**
     * @param position POJO position to save in database
     * @return the saved position if the transaction was a success
     */
    @Override
    @Transactional
    public Position createPosition(Position position) {

        if (existsByLongitudeAndLatitude(position.getLongitude(), position.getLatitude()))
            throw new PositionAlreadyExistsException("This position already exist !");

        var positionEntity = positionRepository.save(positionMapper.toEntity(position));
        return positionMapper.toDto(positionEntity);
    }

    /**
     * @return all the positions in database
     */
    @Override
    public List<Position> getAllPositions() {

        return positionRepository
                .findAll()
                .stream()
                .map(positionMapper::toDto)
                .toList();
    }

    /**
     * @param positionId uuid referenced by the position in database
     * @return the number of deleted row
     */
    @Override
    @Transactional
    public int deletePosition(UUID positionId) {

        return positionRepository
                .findByPositionId(positionId)
                .map(positionEntity -> positionRepository.deleteByPositionId(positionEntity.getPositionId()))
                .orElseThrow(() -> new PositionNotFoundException(POSITION_NOT_FOUND));
    }

    /**
     * @param longitude value of the longitude position
     * @param latitude  value of the latitude position
     * @return true or false follow by the fact that position exist or not.
     */
    @Override
    public boolean existsByLongitudeAndLatitude(BigDecimal longitude, BigDecimal latitude) {

        return positionRepository.existsByLongitudeAndLatitude(longitude, latitude);
    }

    /**
     * @param positionOneId first uuid's position
     * @param positionTwoId second uuid's position
     * @return whether the distance between the two position is less than 10 kilometers or not.
     */
    @Override
    public boolean isDifferenceSmallerThan10Km(UUID positionOneId,
                                               UUID positionTwoId) {

        Position position1 = positionRepository.findByPositionId(positionOneId)
                                               .map(positionEntity -> positionMapper.toDto(positionEntity))
                                               .orElseThrow(() -> new PositionNotFoundException(POSITION_NOT_FOUND));

        Position position2 = positionRepository.findByPositionId(positionTwoId)
                                               .map(positionEntity -> positionMapper.toDto(positionEntity))
                                               .orElseThrow(() -> new PositionNotFoundException(POSITION_NOT_FOUND));

        // Earth's radius in kilometers
        final double R = 6371.0;

        // Convert latitude and longitude from degrees to radians
        BigDecimal firstLongitudeRad = degreeToRadian(position1.getLongitude());
        BigDecimal firstLatitudeRad = degreeToRadian(position1.getLatitude());
        BigDecimal secondLongitudeRad = degreeToRadian(position2.getLongitude());
        BigDecimal secondLatitudeRad = degreeToRadian(position2.getLatitude());

        // Calculate differences between latitudes and longitudes
        BigDecimal differenceLon = secondLongitudeRad.subtract(firstLongitudeRad);
        BigDecimal differenceLat = secondLatitudeRad.subtract(firstLatitudeRad);

        // Haversine formula
        BigDecimal haversine = BigDecimal.valueOf(
                Math.sin(differenceLat.doubleValue() / 2) * Math.sin(differenceLat.doubleValue() / 2) +
                        Math.cos(firstLatitudeRad.doubleValue()) * Math.cos(firstLatitudeRad.doubleValue()) *
                                Math.sin(differenceLon.doubleValue() / 2) * Math.sin(
                                differenceLon.doubleValue() / 2));

        BigDecimal artan = BigDecimal.valueOf(
                2 * Math.atan2(Math.sqrt(haversine.doubleValue()), Math.sqrt(1 - haversine.doubleValue())));

        // Calculate distance
        return BigDecimal.valueOf(R)
                         .multiply(artan)
                         .compareTo(BigDecimal.TEN) <= 0;
    }

    private BigDecimal degreeToRadian(BigDecimal degree) {
        return degree.multiply(BigDecimal.valueOf(Math.PI))
                     .divide(new BigDecimal(180), RoundingMode.HALF_UP);
    }
}
