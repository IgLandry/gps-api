package org.igor.gps.exception;

/**
 * @author Samson Effes
 */

public class PositionAlreadyExistsException extends RuntimeException {

  public PositionAlreadyExistsException(String message) {

    super(message);
  }
}
