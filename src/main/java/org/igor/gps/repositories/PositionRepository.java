package org.igor.gps.repositories;

import org.igor.gps.domain.PositionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PositionRepository extends JpaRepository<PositionEntity, Long> {

    Optional<PositionEntity> findByPositionId(UUID positionId);

    Boolean existsByLongitudeAndLatitude(BigDecimal longitude, BigDecimal latitude);

    int deleteByPositionId(UUID positionId);
}
