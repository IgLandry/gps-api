package org.igor.gps;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(
        info = @Info(
                title = "GPS Spring Boot REST API",
                description = "GPS Spring Boot REST API",
                version = "v1.0",
                contact = @Contact(
                        name = "igor",
                        email = "igorlandry52@yahoo.fr"
                )
        )
)
public class GpsApplication {

    public static void main(String[] args) {

        // http://localhost:8086/swagger-ui/index.html for api documentations
        SpringApplication.run(GpsApplication.class, args);
    }

}
