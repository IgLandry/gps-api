package org.igor.gps.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class Position {

    private UUID positionId;
    @NotBlank
    private String name;
    @NotNull
    private BigDecimal longitude;
    @NotNull
    private BigDecimal latitude;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Position position)) return false;
        return Objects.equals(getName(), position.getName()) && Objects.equals(getLongitude(),
                                                                               position.getLongitude()) && Objects.equals(
                getLatitude(), position.getLatitude());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getLongitude(), getLatitude());
    }
}
