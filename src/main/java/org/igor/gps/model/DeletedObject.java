package org.igor.gps.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public final class DeletedObject {

    private int deletedPositions;
}
