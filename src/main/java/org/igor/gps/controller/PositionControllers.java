package org.igor.gps.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.igor.gps.model.DeletedObject;
import org.igor.gps.model.Position;
import org.igor.gps.services.types.PositionServices;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/positions")
@RequiredArgsConstructor
@CrossOrigin("*")
public class PositionControllers {

    private final PositionServices positionServices;

    @PostMapping
    @Operation(summary = "Create one position")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "success",
                            content = @Content),
                    @ApiResponse(responseCode = "400", description = "bad request", content = @Content),
                    @ApiResponse(responseCode = "403", description = "forbidden", content = @Content)
            })
    public ResponseEntity<Position> createPosition(
            @Validated @RequestBody Position position) {

        final var createdPosition = positionServices.createPosition(position);
        return new ResponseEntity<>(createdPosition, HttpStatus.CREATED);
    }

    @GetMapping
    @Operation(summary = "Get all positions")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "success",
                            content = {
                                    @Content(array = @ArraySchema(schema = @Schema(implementation = Position.class)))
                            }),
                    @ApiResponse(responseCode = "400", description = "bad request", content = @Content),
                    @ApiResponse(responseCode = "403", description = "forbidden", content = @Content)
            })
    public ResponseEntity<List<Position>> getAllPositions() {
        return ResponseEntity.ok(positionServices.getAllPositions());
    }

    @DeleteMapping(value = "/{positionId}")
    @Operation(summary = "Delete one position")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "success",
                            content = @Content),
                    @ApiResponse(responseCode = "400", description = "bad request", content = @Content),
                    @ApiResponse(responseCode = "403", description = "forbidden", content = @Content)
            })
    public ResponseEntity<DeletedObject> deletePosition(@PathVariable UUID positionId) {

        var nbrPositionDeleted = positionServices.deletePosition(positionId);
        return new ResponseEntity<>(new DeletedObject(nbrPositionDeleted), HttpStatus.OK);
    }


    @PostMapping(value = "/distances")
    @Operation(summary = "Distances between two positions")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "success",
                            content = @Content),
                    @ApiResponse(responseCode = "400", description = "bad request", content = @Content),
                    @ApiResponse(responseCode = "403", description = "forbidden", content = @Content)
            })
    public ResponseEntity<Boolean> checkDistance(@RequestParam UUID positionOneId,
                                                 @RequestParam UUID positionTwoId) {
        boolean withInTenKm = positionServices.isDifferenceSmallerThan10Km(positionOneId,
                                                                           positionTwoId);
        return new ResponseEntity<>(withInTenKm, HttpStatus.OK);
    }
}
