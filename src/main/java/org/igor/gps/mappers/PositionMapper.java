package org.igor.gps.mappers;

import org.igor.gps.domain.PositionEntity;
import org.igor.gps.model.Position;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.UUID;

import static org.mapstruct.factory.Mappers.getMapper;

@Mapper(componentModel = "spring")
public interface PositionMapper {

    PositionMapper INSTANCE = getMapper(PositionMapper.class);

    Position toDto(PositionEntity positionEntity);

    @Mapping(target = "version", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdDate", ignore = true)
    @Mapping(target = "positionId", defaultExpression = "java(randomUUID())")
    @Mapping(target = "name", source = "position.name")
    PositionEntity toEntity(Position position);

    default UUID randomUUID() {

        return UUID.randomUUID();
    }
}
