package org.igor.gps.services;

import jakarta.validation.constraints.NotNull;
import org.igor.gps.domain.PositionEntity;
import org.igor.gps.mappers.PositionMapper;
import org.igor.gps.repositories.PositionRepository;
import org.igor.gps.services.implementations.PositionServicesImpl;
import org.igor.gps.services.types.PositionServices;
import org.instancio.Instancio;
import org.instancio.Select;
import org.instancio.junit.InstancioExtension;
import org.instancio.junit.InstancioSource;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith({SpringExtension.class, InstancioExtension.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PositionServiceTest {

    private final PositionMapper positionMapper = PositionMapper.INSTANCE;
    private PositionServices positionServices;
    @MockBean
    private PositionRepository positionRepository;

    @BeforeAll
    void init() {

        final var positionMapper = PositionMapper.INSTANCE;
        positionServices = new PositionServicesImpl(positionRepository, positionMapper);
    }

    @ParameterizedTest
    @InstancioSource
    void givenPositionWhenCallCreatePositionMethodThenReturnTheCreatedOne(PositionEntity positionEntity) {
        // GIVEN

        // WHEN
        when(positionRepository.save(any(PositionEntity.class))).thenReturn(positionEntity);
        var position = positionMapper.toDto(positionEntity);
        var positionCreated = positionServices.createPosition(position);

        // THEN
        assertEquals(position.getPositionId(), positionCreated.getPositionId());
        assertEquals(position.getLongitude(), positionCreated.getLongitude());
        assertEquals(position.getLatitude(), positionCreated.getLatitude());
    }

    @ParameterizedTest
    @InstancioSource
    void givenSomePositionsWhenCallMethodGetAllPositionThenReturnThem(PositionEntity positionEntity) {
        // GIVEN
        // WHEN
        when(positionRepository.findAll()).thenReturn(List.of(positionEntity));
        // THEN
        var positions = positionServices.getAllPositions();
        assertEquals(1, positions.size());
    }

    @ParameterizedTest
    @InstancioSource
    void givenPositionUuidWhenCallMethodDeletePositionThenWorkingWell(@NotNull PositionEntity positionEntity) {
        // GIVEN
        var positionId = positionEntity.getPositionId();
        // WHEN
        when(positionRepository.findByPositionId(positionId)).thenReturn(Optional.of(positionEntity));
        when(positionRepository.deleteByPositionId(positionId)).thenReturn(1);
        // THEN
        var nbrOfDeletedPosition = positionServices.deletePosition(positionId);
        assertEquals(1, nbrOfDeletedPosition);
    }

    @Test
    @InstancioSource
    void givenTwoPositionsReturnTrueWhenThereOverThan10kmBetweenEachOthers() {
        // GIVEN
        final var positionEntity1 = Instancio.of(PositionEntity.class)
                                             .set(Select.field("positionId"), UUID.randomUUID())
                                             .set(Select.field("longitude"), BigDecimal.valueOf(2.2384))
                                             .set(Select.field("latitude"), BigDecimal.valueOf(48.8918))
                                             .create();

        final var positionEntity2 = Instancio.of(PositionEntity.class)
                                             .set(Select.field("positionId"), UUID.randomUUID())
                                             .set(Select.field("longitude"), BigDecimal.valueOf(2.2981))
                                             .set(Select.field("latitude"), BigDecimal.valueOf(48.8559))
                                             .create();
        UUID id1 = positionEntity1.getPositionId();
        UUID id2 = positionEntity2.getPositionId();
        // WHEN

        when(positionRepository.findByPositionId(id1)).thenReturn(
                Optional.of(positionEntity1));
        when(positionRepository.findByPositionId(id2)).thenReturn(
                Optional.of(positionEntity2));
        // THEN
        assertTrue(positionServices.isDifferenceSmallerThan10Km(id1, id2));

    }
}
